<?php

    require "config/config.php";

    $id = $_POST['id'];

    $delete = $bdd->prepare('DELETE FROM users WHERE id = :id');
    $delete->bindParam(':id', $id, PDO::PARAM_INT);
    $delete->execute();

    header('Location: index.php');
    exit();