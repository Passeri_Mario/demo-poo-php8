<?php

    class Employe{
        private $age;
        private $name;
        private $status;

        /**
         * @param $age
         * @param $name
         * @param $status
         * @throws Exception
         */
        public function __construct($age, $name, $status)
        {
            $this->setAge($age);
            $this->name = $name;
            $this->status = $status;
        }

        public function Presentation(): void {
            echo '<p> Nom : ' . $this->getName() . '</p>';
            echo '<p> Age : ' . $this->getAge() . ' ans</p>';
            echo '<p> Poste : ' . $this->getStatus(). '</p>';
        }

        /**
         * @return mixed
         */
        public function getAge()
        {
            return $this->age;
        }

        /**
         * @param mixed $age
         * @throws Exception
         */
        public function setAge($age)
        {
            if (is_string($age)) {
                $this->age = $age;
            } else{
                throw new \RuntimeException('Erreur sur l\'age de l\'employé');
            }
        }

        /**
         * @return mixed
         */
        public function getName()
        {
            return $this->name;
        }

        /**
         * @param mixed $name
         */
        public function setName($name): void
        {
            if (is_string($name)) {
                $this->name = $name;
            } else{
                throw new \RuntimeException('Erreur sur le nom de l\'employé');
            }
        }

        /**
         * @return mixed
         */
        public function getStatus()
        {
            return $this->status;
        }

        /**
         * @param mixed $status
         */
        public function setStatus($status)
        {
            if (is_string($status)) {
                return $this->status;
            } else{
                throw new \RuntimeException('Erreur sur le status de l\'employé');
            }
        }
    }