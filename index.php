<?php
    require "config/config.php";
    require 'class/employe.php';
    require 'class/technicien.php';
?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <title>Exercice POO</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
        <link href="assets/css/style.css" rel="stylesheet">
    </head>
    <body class="container mt-5">

        <header>
            <h1 class="mt-5 mb-5">Liste du personnel</h1>
            <nav class="navbar bg-light mt-5 mb-5">
                <div class="container-fluid">
                    <a class="navbar-brand" href="index.php">
                        <img src=assets/img/logo.jpg alt="logo applicative" width="50" height="50" class="d-inline-block align-text-top">
                        Exercice et démonstration en POO en PHP 8
                    </a>
                </div>
            </nav>
        </header>

        <main>

            <button type="button" class="btn btn-primary mt-1 mb-5" data-bs-toggle="modal" data-bs-target="#modalAdd">
                Ajouter du personel
            </button>

            <div class="modal fade" id="modalAdd" tabindex="-1" aria-labelledby="modalAdd" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form method="post" action="add.php">
                            <div class="modal-body">
                                <div class="mb-3">
                                    <label for="nom" class="form-label">Nom *</label>
                                    <input required="required" type=text class="form-control" id="nom" name="nom">
                                </div>
                                <div class="mb-3">
                                    <label for="age" class="form-label">Age *</label>
                                    <input  required="required" type="number" min="16" max="70" class="form-control" id="age" name="age">
                                </div>
                                <div class="mb-3">
                                    <label for="poste" class="form-label">Poste *</label>
                                    <input  required="required" type="text" class="form-control" id="poste" name="poste">
                                </div>
                                <div class="mb-3">
                                    <label for="specialisation" class="form-label">Specialiation</label>
                                    <input type="text" class="form-control" id="specialisation" name="specialisation">
                                </div>
                            </div>
                            <div class="d-grid gap-2 container mt-1 mb-4">
                                <button type="submit" class="btn btn-success">Envoyer</button>
                                <button type="reset" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <?php include "sql/getusers.php" ?>

        </main>

        <footer>
            <div class="alert alert-info mt-5 mb-5" role="alert">
                <p>Code disponible sur Gitlab - Passeri Mario <br>
                    <a target="_blank" href="https://gitlab.com/Passeri_Mario/demo-poo-php8">https://gitlab.com/Passeri_Mario/demo-poo-php8</a>
                </p>
            </div>
        </footer>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
    </body>
</html>
