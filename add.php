<?php

    require "config/config.php";

    htmlentities($null = null);
    htmlentities($nom = $_POST['nom']);
    htmlentities($age = $_POST['age']);
    htmlentities($poste = $_POST['poste']);
    htmlentities($specialisation = $_POST['specialisation']);

    $insert = $bdd->prepare('INSERT INTO users ( id, name, age, poste, specialisation) VALUES(?, ?, ?, ?, ?)');
    $insert->execute(array($null, htmlentities($nom), htmlentities($age), htmlentities($poste), htmlentities($specialisation)));

    header('Location: index.php');
    exit();