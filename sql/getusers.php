<?php

    // technicien
    $usersTechnicien = $bdd->query('SELECT * FROM users WHERE specialisation != ""');
    $resultsTechnicien = $usersTechnicien->fetchAll(PDO::FETCH_ASSOC);

    foreach ($resultsTechnicien as $rowTechnicien) {
        echo ' <div class="alert alert-primary">';
        echo '<form class="position-absolute top-0 end-0 me-3 mt-3" method="post" action="delete.php">
                <input type="hidden" value="'.$rowTechnicien['id'].'" name="id">
                <button type="submit" class="btn btn-danger">Supprimer</button>
              </form>';
        $info_techicien = new Technicien($rowTechnicien['age'], $rowTechnicien['name'], $rowTechnicien['poste'], $rowTechnicien['specialisation']);
        $info_techicien->Presentation();
        echo '</div>';
    }

    // employe
    $usersEmploye = $bdd->query('SELECT * FROM users WHERE specialisation = ""');
    $resultsEmploye = $usersEmploye->fetchAll(PDO::FETCH_ASSOC);

    foreach ($resultsEmploye as $rowEmploye) {
        echo ' <div class="alert alert-primary">';
        echo '<form class="position-absolute top-0 end-0 me-3 mt-3" method="post" action="delete.php">
                <input type="hidden" value="'.$rowEmploye['id'].'" name="id">
                <button type="submit" class="btn btn-danger">Supprimer</button>
              </form>';
        $info_users = new Employe($rowEmploye['age'], $rowEmploye['name'], $rowEmploye['poste']);
        $info_users->Presentation();
        echo '</div>';
    }
