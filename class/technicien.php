<?php

    class Technicien extends Employe{

        public $specialsiation;

        /**
         * @param $specialsiation
         */
        public function __construct($age, $name, $status, $specialsiation)
        {
            // appel du parent Employe
            parent::__construct($age, $name, $status);
            $this->specialsiation = $specialsiation;
        }

        public function Presentation(): void {
            echo '<p> Nom : ' . $this->getName() . '</p>';
            echo '<p> Age : ' . $this->getAge() . ' ans</p>';
            echo '<p> Poste : ' . $this->getStatus(). '</p>';
            echo '<p> Spécialsiation : ' . $this->getSpecialsiation(). '</p>';
        }

        /**
         * @return mixed
         */
        public function getSpecialsiation()
        {
            return $this->specialsiation;
        }

        /**
         * @param mixed $specialsiation
         */
        public function setSpecialsiation($specialsiation): void
        {
            if (is_string($specialsiation)) {
            $this->specialsiation = $specialsiation;
            }
            else{
                throw new \RuntimeException('Erreur sur la specialsiation');
            }
        }
    }
